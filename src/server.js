const schedule = require('node-schedule')
const { statsSchedule } = require('./app/schedule/statsSchedule')
const app = require('./config/app')
const { port, address } = require('./config/environments').server
const { hostPrint } = require('./config/shared/prints')

schedule.scheduleJob('*/1 * * * *', statsSchedule)

app.listen(3000, hostPrint(port, address))
