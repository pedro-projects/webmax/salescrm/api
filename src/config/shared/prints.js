const hostPrint = (port, address) => {
  console.log('[HOST] Server ON!')
  console.log(
    `[HOST] Server is running on: \u001b[32mhttp://${address}:${port}\x1b[0m`
  )
  console.log(
    `[HOST] If server running on \x1b[36mdocker\x1b[0m, the host is: \u001b[32mhttp://127.0.0.1:${port}\x1b[0m`
  )
}

module.exports = {
  hostPrint
}
