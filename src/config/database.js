const { postgres } = require('./environments')

module.exports = {
  host: postgres.host,
  username: postgres.username,
  password: postgres.password,
  database: postgres.database,
  dialect: postgres.dialect,
  logging: false,
  define: postgres.define
}
