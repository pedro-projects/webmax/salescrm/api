const user = require('../app/controllers/UserController')
const { auth, validateToken } = require('../app/controllers/AuthController')
const { store, index } = require('../app/controllers/PeopleController')
const routes = require('express').Router()

const { Stat } = require('../app/mongo/Stats')

// Auth Routes
routes.post('/auth', auth)
routes.post('/validateToken', validateToken)

// People
routes.get('/people', index)
routes.post('/people', store)

// Users Routes
routes.post('/users', user.save)
routes.get('/users', user.index)

routes.get('/', (req, res, next) => {
  res.json({ message: 'Hello World' })

  return next()
})

routes.get('/stats', (req, res, next) => {
  Stat.findOne({}, {}, { sort: { createdAt: -1 } }).then((stat) => {
    const defaultStat = {
      users: 0,
      peoples: 0,
      companies: 0
    }
    res.json(stat || defaultStat)
  })
})

module.exports = routes
