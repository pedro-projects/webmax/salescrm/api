require('dotenv').config()

module.exports = {
  server: {
    port: process.env.PORT || 3000,
    address: process.env.ADDRESS || '127.0.0.1',
    authSecret: process.env.AUTH_SECRET || 'Auth'
  },
  postgres: {
    host: process.env.DB_HOST || '127.0.0.1',
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    database: process.env.DB_DATABASE || 'postgres',
    dialect: process.env.DB_DIALECT || 'postgres',
    define: {
      timestamps: true,
      underscored: true,
      underscoredAll: true
    }
  }
}
