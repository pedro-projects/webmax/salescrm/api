const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
require('../app/schedule/statsSchedule')

class AppController {
  constructor() {
    this.express = express()

    this.middlewares()
    this.routes()
  }

  middlewares() {
    this.express.use(express.json())
    this.express.use(cors({ origin: '*' }))
    this.express.use(morgan('common'))
  }

  routes() {
    this.express.use(require('./routes'))
  }
}

module.exports = new AppController().express
