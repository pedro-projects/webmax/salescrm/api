module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      user_name: DataTypes.STRING,
      password: DataTypes.STRING,
      type: DataTypes.STRING
    },
    {
      tableName: 'user'
    }
  )

  User.associate = (models) => {
    User.belongsTo(models.People, { foreignKey: 'id', as: 'people' })
  }

  return User
}
