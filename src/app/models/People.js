module.exports = (sequelize, DataTypes) => {
  const People = sequelize.define(
    'People',
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      cpf: DataTypes.STRING,
      birthday: DataTypes.DATE,
      rg: DataTypes.STRING,
      mother_name: DataTypes.STRING,
      gender: DataTypes.STRING,
      avatar_url: DataTypes.STRING
    },
    {
      tableName: 'people'
    }
  )

  People.associate = (models) => {
    People.hasOne(models.User, { foreignKey: 'id', as: 'user' })
  }

  return People
}
