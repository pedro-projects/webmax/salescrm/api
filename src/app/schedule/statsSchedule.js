const { User, People } = require('../models')

module.exports = {
  async statsSchedule() {
    const usersCount = await User.count()
    const peopleCount = await People.count()

    const { Stat } = require('../mongo/Stats')

    const lastStat = await Stat.findOne({}, {}, { sort: { createdAt: -1 } })

    const stat = new Stat({
      users: usersCount,
      peoples: peopleCount,
      createdAt: new Date()
    })

    const changeUsers = !lastStat || stat.users !== lastStat.users
    const changePeoples = !lastStat || stat.peoples !== lastStat.peoples

    if (changeUsers || changePeoples) {
      stat.save().then(() => console.log('[Stats] Estatíticas atualizadas!'))
    }
  }
}
