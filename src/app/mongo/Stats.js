const mongoose = require('mongoose')

mongoose
  .connect('mongodb://localhost/atlas_stats', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('Conectado')
  })
  .catch((e) => {
    const msg = 'ERRO! Não foi possível conectar com o MongoDB!'
    console.log('\x1b[41m%s\x1b[37m', msg, '\x1b[0m')
  })

const Stat = mongoose.model('Stat', {
  users: Number,
  peoples: Number,
  companies: Number,
  total_sales: Number,
  phones: Number,
  address: Number,
  total_leads: Number,
  createdAt: Date
})

module.exports = {
  Stat
}
