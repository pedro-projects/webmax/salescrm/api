// const bcrypt = require('bcrypt-nodejs')

const { User, People } = require('../models')

// const {
//   existsOrError,
//   equalsOrError,
//   notExistsOrError
// } = require('../../config/shared/validation')

// const encryptPassword = (password) => {
//   const salt = bcrypt.genSaltSync(10)
//   return bcrypt.hashSync(password, salt)
// }

module.exports = {
  async save(req, res, next) {
    const { people, userName, password, type } = req.body

    const peopleFromDB = await People.findByPk(people)

    if (!peopleFromDB) {
      return res.status(400).json({ error: 'People not found' })
    }

    const user = await User.create({
      user_name: userName,
      password,
      type
    })

    return res.json(user)
  },

  async index(req, res, next) {
    const users = await User.findAll({
      include: {
        association: 'people'
      }
    })

    return res.json(users)
  }
}
