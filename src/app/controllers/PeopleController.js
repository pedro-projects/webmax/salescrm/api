const { People } = require('../models')

module.exports = {
  async index(req, res, next) {
    const people = await People.findAll({
      include: {
        association: 'user'
      }
    })

    return res.json(people)
  },

  async store(req, res, next) {
    const {
      name,
      email,
      cpf,
      birthday,
      rg,
      motherName,
      gender,
      avatarUrl
    } = req.body

    const people = await People.create({
      name,
      email,
      cpf,
      birthday,
      rg,
      mother_name: motherName,
      gender,
      avatar_url: avatarUrl
    })

    return res.json(people)
  }
}
