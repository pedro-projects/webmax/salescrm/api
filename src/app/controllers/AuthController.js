const bcrypt = require('bcrypt-nodejs')
const { encode, decode } = require('jwt-simple')
const { User } = require('../models')
const { authSecret } = require('../../config/environments').server

module.exports = {
  async auth(req, res, next) {
    if (!req.body.email || !req.body.password) {
      res.status(400).json({ error: 'Informe e-mail e senha!' })
      return next()
    }
    const user = await User.findOne({
      where: {
        email: req.body.email
      }
    })

    if (!user) {
      res.status(400).json({ error: 'Usuário não encontrado!' })
      return next()
    }

    const isMatch = bcrypt.compareSync(req.body.password, user.password)
    if (!isMatch) {
      res.status(401).json({ error: 'E-mail ou senha inválidos!' })
      return next()
    }

    const now = Math.floor(Date.now() / 1000)

    const payload = {
      user: {
        id: user.id,
        name: user.name,
        email: user.email,
        type: user.type,
        avatar_url: user.avatar_url
      },
      iat: now,
      exp: now + 60 * 60 * 24 * 3
    }
    res.json({
      ...payload,
      token: encode(payload, authSecret)
    })
  },

  async validateToken(req, res, next) {
    const userData = req.body || null

    try {
      if (userData) {
        const token = decode(userData.token, authSecret)

        if (new Date(token.exp * 1000) > new Date()) {
          return res.send(true)
        }
      }
    } catch (error) {}

    res.send(false)
  }
}
