'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('car', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      model: {
        type: Sequelize.INTEGER,
        references: { model: 'model_car', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      owner: {
        type: Sequelize.INTEGER,
        references: { model: 'people', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      year: {
        type: Sequelize.DATE
      },
      car_plate: {
        type: Sequelize.STRING
      },
      chassis: {
        type: Sequelize.STRING
      },
      insurance: {
        type: Sequelize.BOOLEAN
      },
      for_work: {
        type: Sequelize.BOOLEAN
      },
      fip: {
        type: Sequelize.FLOAT
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('car')
  }
}
