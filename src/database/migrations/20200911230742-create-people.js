'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('people', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false
      },
      cpf: {
        type: Sequelize.STRING,
        unique: true
      },
      birthday: {
        type: Sequelize.DATE
      },
      rg: {
        type: Sequelize.STRING
      },
      mother_name: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.STRING
      },
      avatar_url: {
        type: Sequelize.STRING,
        defaultValue: 'https://cdn.quasar.dev/logo/svg/quasar-logo.svg',
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('people')
  }
}
