'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('employee_contract', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      employee: {
        type: Sequelize.INTEGER,
        references: { model: 'people', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      companie: {
        type: Sequelize.INTEGER,
        references: { model: 'companie', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      type_contract: {
        type: Sequelize.INTEGER
      },
      base_salary: {
        type: Sequelize.FLOAT
      },
      role: {
        type: Sequelize.STRING
      },
      departament: {
        type: Sequelize.STRING
      },
      admission: {
        type: Sequelize.DATE
      },
      termination: {
        type: Sequelize.DATE
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('employee_contract')
  }
}
