'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('companie', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      company_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      fantasy_name: {
        type: Sequelize.STRING
      },
      head_office: {
        type: Sequelize.BOOLEAN
      },
      cnae: {
        type: Sequelize.STRING
      },
      mei: {
        type: Sequelize.BOOLEAN
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false
      },
      avatar_url: {
        type: Sequelize.STRING,
        defaultValue: 'https://cdn.quasar.dev/logo/svg/quasar-logo.svg',
        allowNull: false
      },
      person_contact: {
        type: Sequelize.INTEGER,
        references: { model: 'people', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('companie')
  }
}
