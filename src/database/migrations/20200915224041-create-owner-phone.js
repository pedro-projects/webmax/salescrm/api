'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('owner_phone', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      people: {
        type: Sequelize.INTEGER,
        references: { model: 'people', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      companie: {
        type: Sequelize.INTEGER,
        references: { model: 'companie', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      phone: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'phone', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('owner_phone')
  }
}
